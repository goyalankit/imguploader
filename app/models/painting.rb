class Painting < ActiveRecord::Base
  mount_uploader :image, PaintingUploader

  validates :name, presence: true
end
